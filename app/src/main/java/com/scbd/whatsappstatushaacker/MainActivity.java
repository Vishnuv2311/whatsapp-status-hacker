package com.scbd.whatsappstatushaacker;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {
    ListView lv;

    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media/.Statuses";
    File directory = new File(path);
    File[] files = directory.listFiles();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            rePer();
        }


try{


        final String[] theNamesOfFiles = new String[files.length];
       final String[] filepath = new String[directory.getAbsolutePath().length()];


        lv = (ListView) findViewById(R.id.idListView);


        for (int i = 0; i < files.length; i++) {

            theNamesOfFiles[i] = files[i].getName();
               filepath[i]=files[i].getAbsolutePath();

        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, theNamesOfFiles);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//               Uri path=Uri.fromFile(new File(filepath[i]));
                Intent intent=new Intent(Intent.ACTION_VIEW);
                File file=new File(filepath[i]);
                Uri fileUri=Uri.fromFile(file);
                intent.setDataAndType(fileUri,"*/*");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                try{
                    Intent chooser=Intent.createChooser(intent,"Choose Applicaion");
                    startActivity(chooser);
                }
                catch (ActivityNotFoundException e){
                    Toast.makeText(MainActivity.this,"No Availabe to Open file",Toast.LENGTH_LONG).show();
                }
               // intent.setAction(Uri.withAppendedPath(path),"images/*");
               // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //startActivity(intent);
                ///Toast.makeText(MainActivity.this,theNamesOfFiles[i]+"\n"+filepath[i],Toast.LENGTH_LONG).show();
            }
        });
}catch (Exception e){
return;
}

    }

    private void rePer() {
        ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
           }

}





